# DopamineSampleProject

How to start the Project:

First: Go to the angular client folder open it with terminal write: "npm install".
Second: Go to the Nest api folder open it with termiral and write: "npm install"

These two comands will download or update the needed pacages.

Third: You need to go to the angular terminal from step one and write: "ng serve" - will start the frontend.
Fourth: Go to the terminal from the second step and write: "npm run start:dev" - will start the backend.

If there are any questions I'm online on denislazarov2@gmail.com.