import {
  Component,
  OnInit,
  AfterViewInit,
  AfterContentInit
} from "@angular/core";
import { NotificationsService } from "src/app/core/services/notification.service";

@Component({
  selector: "app-notifications",
  templateUrl: "./notifications.component.html",
  styleUrls: ["./notifications.component.css"]
})
export class NotificationsComponent implements OnInit {
  notifications: any = [];
  notificationToPost: any = {};
  id = "";
  title = "";
  type = "";
  text = "";

  constructor(private readonly notificationsService: NotificationsService) {}

  ngOnInit() {
    this.notificationsService.getNotifications().subscribe(data => {
      this.notifications = data;

      console.log(this.notifications);
      this.notifications.forEach(notification => {
        if (notification.expires) {
          setTimeout(() => {
            this.notifications = this.notifications.filter(
              element => element !== notification
              );
              this.notificationsService.deleteNotification(notification.id).subscribe();
          }, notification.expires);
        }
      });
    });
  }

  notificationCount(): number {
    const bonusNotifications = [];
    this.notifications.forEach(element => {
      if (element.type == "bonus") {
        bonusNotifications.push(element);
      }
    });
    return this.notifications.length - bonusNotifications.length;
  }

  postNotification() {
    this.notificationToPost = {
      id: this.id,
      title: this.title,
      type: this.type,
      text: this.text
    };
    this.notificationsService
      .postNotification(this.notificationToPost)
      .subscribe(notification => this.notifications.push(notification));
  }
}
