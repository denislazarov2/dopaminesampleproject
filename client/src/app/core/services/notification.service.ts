import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
  })
  export class NotificationsService {
    constructor(private readonly http: HttpClient) {}

    getNotifications() {
        return this.http.get('http://localhost:3000/notifications')
    }

    postNotification(notification) {
      return this.http.post('http://localhost:3000/notifications', notification);
    }

    deleteNotification(notificationId) {
      return this.http.delete<any>(`http://localhost:3000/notifications/${notificationId}`)
    }
}
