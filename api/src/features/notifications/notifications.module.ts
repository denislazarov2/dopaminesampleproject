import { NotificationsService } from "./notifications.service";
import { NotificationsController } from "./notifications.controller";
import { Module } from "@nestjs/common";

@Module({
    imports: [],
    providers: [NotificationsService],
    controllers: [NotificationsController],
    exports: [NotificationsService],
  })
  export class NotificationsModule {}
  