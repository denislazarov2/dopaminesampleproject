import { Injectable } from "@nestjs/common";
import { data } from '../../database/data'
import e = require("express");
@Injectable()
export class NotificationsService {

  constructor() {}

  async getNotifications() {
    return data;
  }

  async postNewNotification(notification) {
    data.push(notification);
    return notification
  }

  async deleteNotification(notificationId) {
    const indexOfNotification = data.findIndex(element => notificationId == element.id)
    data.splice(indexOfNotification, 1)
  }
}
