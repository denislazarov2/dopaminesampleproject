import { Controller, Get, Post, Body, Delete, Param } from "@nestjs/common";
import { NotificationsService } from "./notifications.service";


@Controller('notifications')
export class NotificationsController {
  constructor( private readonly notificatonsService: NotificationsService) {}

  @Get()
  async getNotifications() {
      return this.notificatonsService.getNotifications();
  }

  @Post()
  async postNotification(@Body()notification) {
    return this.notificatonsService.postNewNotification(notification)
  }

  @Delete('/:notificationId')
  async deleteNotification(@Param('notificationId') notificatinId) {
   return this.notificatonsService.deleteNotification(notificatinId)
  }
}